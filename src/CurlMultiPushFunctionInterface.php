<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlMultiPushFunctionInterface interface file.
 * 
 * This class represents a callable that will be used to handle pushed handle
 * from an HTTP/2 connection handle.
 * 
 * @author Anastaszor
 */
interface CurlMultiPushFunctionInterface
{
	
	/**
	 * This function is called when the server pushes additional handle to
	 * this specific handle.
	 * 
	 * @param CurlInterface $parentHandle The request the client made
	 * @param CurlInterface $pushedHandle The pushed response from the server
	 * @param array<integer, string> $headers the pushed promise headers
	 * @return integer CURL_PUSH_OK if it can handle the push, or CURL_PUSH_DENY
	 *                 to reject it
	 */
	public function accept(
		CurlInterface $parentHandle,
		CurlInterface $pushedHandle,
		array $headers = []
	) : int;
	
}
