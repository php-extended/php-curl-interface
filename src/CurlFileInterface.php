<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlFileInterface class file.
 * 
 * This interface specifies a file handle to be uploaded.
 * 
 * @author Anastaszor
 */
interface CurlFileInterface
{
	
	/**
	 * Gets the file name of this file.
	 * 
	 * @return string
	 */
	public function getFilename() : string;
	
	/**
	 * Gets the mime type of this file.
	 * 
	 * @return string
	 */
	public function getMimeType() : string;
	
	/**
	 * Gets the post attribute name that will be used to post this file.
	 * 
	 * @return string
	 */
	public function getPostFilename() : string;
	
}
