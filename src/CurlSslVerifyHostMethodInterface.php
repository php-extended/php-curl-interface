<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlSslVerifyHostMethodInterface interface file.
 * 
 * This class represents the curl ssl verify host methods that are allowed in
 * curl.
 * 
 * @author Anastaszor
 */
interface CurlSslVerifyHostMethodInterface
{
	
	/**
	 * Gets the curl constant value.
	 * 
	 * @return integer
	 */
	public function getCurlValue() : int;
	
}
