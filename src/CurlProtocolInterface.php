<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlProtocolInterface interface file.
 * 
 * This interface represents the protocols that are allowed in curl.
 * 
 * @author Anastaszor
 */
interface CurlProtocolInterface
{
	
	/**
	 * Gets the curl constant value.
	 * 
	 * @return integer
	 */
	public function getCurlValue() : int;
	
	/**
	 * Merges with the other protocol and returns the result of the merge. 
	 * 
	 * @param CurlProtocolInterface $other
	 * @return CurlProtocolInterface
	 */
	public function with(CurlProtocolInterface $other) : CurlProtocolInterface;
	
}
