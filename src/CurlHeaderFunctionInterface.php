<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlHeaderFunctionInterface interface file.
 * 
 * This interface specifies a callback to write header data that is transferred
 * from a curl instance.
 * 
 * @author Anastaszor
 */
interface CurlHeaderFunctionInterface
{
	
	/**
	 * This function executes the writing of the header data from the curl
	 * object.
	 * 
	 * @param CurlInterface $curl
	 * @param string $headerStr
	 * @return integer the number of bytes written
	 */
	public function write(CurlInterface $curl, string $headerStr) : int;
	
}
