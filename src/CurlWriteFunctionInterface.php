<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlWriteFunctionInterface interface file.
 * 
 * This interface specifies a callback to write data that is transferred from
 * a curl instance.
 * 
 * @author Anastaszor
 */
interface CurlWriteFunctionInterface
{
	
	/**
	 * This function executes the writing of the data from the curl object.
	 * 
	 * @param CurlInterface $curl
	 * @param string $data
	 * @return integer the number of bytes written
	 */
	public function write(CurlInterface $curl, string $data) : int;
	
}
