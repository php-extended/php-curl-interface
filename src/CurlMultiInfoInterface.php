<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlMultiInfoInterface class file.
 * 
 * This interface specifies a wrapper around the return of the
 * curl_multi_info_read function results.
 * 
 * @author Anastaszor
 */
interface CurlMultiInfoInterface
{
	
	/**
	 * The message for the status.
	 * 
	 * @return string
	 */
	public function getMessage() : string;
	
	/**
	 * The result of the info, will be one of the CURLE_* constants, and 
	 * CURLE_OK if there is no errors.
	 * 
	 * @return integer
	 */
	public function getResult() : int;
	
	/**
	 * The curl multi handle that is concerned.
	 * 
	 * @return CurlInterface
	 */
	public function getHandle() : CurlInterface;
	
}
