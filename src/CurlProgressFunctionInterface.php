<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlProgressFunctionInterface interface file.
 * 
 * This inerface specifies a callback to show the progress made on the base
 * of data that is given by the curl instance.
 * 
 * @author Anastaszor
 */
interface CurlProgressFunctionInterface
{
	
	/**
	 * This function executes the display of progress information on the base
	 * of the given information.
	 * 
	 * @param CurlInterface $curl
	 * @param integer $totalDownloadBytes
	 * @param integer $actualDownloadBytes
	 * @param integer $totalUploadBytes
	 * @param integer $actualUploadBytes
	 * @return integer 0 to continue, non-zero to abort the transfer
	 */
	public function progress(
		CurlInterface $curl,
		int $totalDownloadBytes,
		int $actualDownloadBytes,
		int $totalUploadBytes,
		int $actualUploadBytes
	) : int;
	
}
