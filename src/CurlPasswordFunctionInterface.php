<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlPasswordFunctionInterface interface file.
 * 
 * This interface specifies a callback to prompt for passwords on files that
 * are used by a curl instance.
 * 
 * @author Anastaszor
 */
interface CurlPasswordFunctionInterface
{
	
	/**
	 * This function executes the prompt for a password according to the given
	 * prompt string and the maximum length of the password.
	 * 
	 * @param CurlInterface $curl
	 * @param string $prompt
	 * @param integer $maxlength
	 * @return string the raw password
	 */
	public function prompt(CurlInterface $curl, string $prompt, int $maxlength) : string;
	
}
