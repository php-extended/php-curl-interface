<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

use DateTimeInterface;
use PhpExtended\Ip\Ipv4AddressInterface;
use PhpExtended\Ip\Ipv6AddressInterface;
use PhpExtended\UserAgent\UserAgentInterface;
use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UriInterface;

/**
 * CurlInterface interface file.
 * 
 * This interface represents the main object of curl : a session to fetch a web
 * page.
 * 
 * @author Anastaszor
 */
interface CurlInterface
{
	
	/**
	 * Executes the current curl request.
	 * 
	 * @return boolean
	 */
	public function execute() : bool;
	
	/**
	 * Gets the data of the request that was made.
	 * 
	 * @return StreamInterface
	 */
	public function getData() : StreamInterface;
	
	/**
	 * Gets the latest error code.
	 * 
	 * @return integer
	 */
	public function getErrorCode() : int;
	
	/**
	 * Gets the latest error message.
	 * 
	 * @return string
	 */
	public function getErrorMessage() : string;
	
	/**
	 * Enables to automatically set the Referer: field in requests where it
	 * follows a Location: redirect. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableAutoreferer() : bool;
	
	/**
	 * Disables the auto referer.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableAutoreferer() : bool;
	
	/**
	 * Enables to mark this as a new cookie "session". It will force libcurl to
	 * ignore all cookies it is about to load that are "session cookies" from
	 * the previous session. By default, libcurl always stores and loads all
	 * cookies, independent if they are session cookies or not. Session cookies
	 * are cookies without expiry date and they are meant to be alive and
	 * existing for this "session" only. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableCookieSession() : bool;
	
	/**
	 * Disables the cookie sessions.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableCookieSession() : bool;
	
	/**
	 * Enables to output SSL certification information to STDERR on secure transfers. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableCertInfo() : bool;
	
	/**
	 * Disables certification information output.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableCertInfo() : bool;
	
	/**
	 *  Enables tells the library to perform all the required proxy
	 *  authentication and connection setup, but no data transfer. This option
	 *  is implemented for HTTP, SMTP and POP3. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableConnectOnly() : bool;
	
	/**
	 * Disables connect only.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableConnectOnly() : bool;
	
	/**
	 * Enables to convert Unix newlines to CRLF newlines on transfers. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableCrlf() : bool;
	
	/**
	 * Disables to convert Unix newlines to CRLF newlines on transfers. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableCrlf() : bool;
	
	/**
	 * Enables to use a global DNS cache. This option is not thread-safe and is
	 * enabled by default. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableDnsUseGlobalCache() : bool;
	
	/**
	 * Disables global DNS cache.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableDnsUseGlobalCache() : bool;
	
	/**
	 * Enables to fail verbosely if the HTTP code returned is greater than or
	 * equal to 400. The default behavior is to return the page normally,
	 * ignoring the code. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableFailOnError() : bool;
	
	/**
	 * Disables the fail on http error codes.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableFailOnError() : bool;
	
	/**
	 * Enables TLS false start. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableSslFalseStart() : bool;
	
	/**
	 * Disables TLS false start.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableSslFalseStart() : bool;
	
	/**
	 * Enables to attempt to retrieve the modification date of the remote document.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableFiletime() : bool;
	
	/**
	 * Disables to attempt to retrieve the modification date of the remote document.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableFiletime() : bool;
	
	/**
	 * Enables to follow any "Location: " header that the server sends as part
	 * of the HTTP header (note this is recursive, PHP will follow as many
	 * "Location: " headers that it is sent, unless CURLOPT_MAXREDIRS is set). 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableFollowLocation() : bool;
	
	/**
	 * Disables automatic location following.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableFollowLocation() : bool;
	
	/**
	 * Enables to force the connection to explicitly close when it has finished
	 * processing, and not be pooled for reuse. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableForbidReuse() : bool;
	
	/**
	 * Disables forced connection close.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableForbidReuse() : bool;
	
	/**
	 * Enables to force the use of a new connection instead of a cached one. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableFreshConnect() : bool;
	
	/**
	 * Disables to force the use of a new connection instead of a cached one. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableFreshConnect() : bool;
	
	/**
	 * Enables to use EPRT (and LPRT) when doing active FTP downloads. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableFtpUseEprt() : bool;
	
	/**
	 * Disables EPRT and LPRT and use PORT only when doing active FTP downloads.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableFtpUseEprt() : bool;
	
	/**
	 * Enables to first try an EPSV command for FTP transfers before reverting
	 * back to PASV.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableFtpUseEpsv() : bool;
	
	/**
	 * Disables EPSV commands.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableFtpUseEpsv() : bool;
	
	/**
	 * Enables to create missing directories when an FTP operation encounters
	 * a path that currently doesn't exist. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableFtpCreateMissingDirs() : bool;
	
	/**
	 * Disables automatic generation of missing directories.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableFtpCreateMissingDirs() : bool;
	
	/**
	 * Enables to append to the remote file instead of overwriting it. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableFtpAppend() : bool;
	
	/**
	 * Disables to append to the remote file.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableFtpAppend() : bool;
	
	/**
	 * Enables to disable TCP's Nagle algorithm, which tries to minimize the
	 * number of small packets on the network. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableTcpNodelay() : bool;
	
	/**
	 * Disables Nagle algorithm.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableTcpNodelay() : bool;
	
	/**
	 * Enables to only list the names of an FTP directory. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableFtpListOnly() : bool;
	
	/**
	 * Disables directory listing only.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableFtpListOnly() : bool;
	
	/**
	 * Enables to include the headers in the output. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableHeader() : bool;
	
	/**
	 * Disables to include the headers in the output. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableHeader() : bool;
	
	/**
	 * Enables to track the handle's request string. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableHeaderOut() : bool;
	
	/**
	 * Disables to track the handle's request string. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableHeaderOut() : bool;
	
	/**
	 * Enables to tunnel through a given HTTP proxy. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableHttpProxyTunnel() : bool;
	
	/**
	 * Disables to tunnel through a given HTTP proxy. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableHttpProxyTunnel() : bool;
	
	/**
	 * Enables to scan the ~/.netrc file to find a username and password for
	 * the remote site that a connection is being established with. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableNetrc() : bool;
	
	/**
	 * Disables scanning for ~/.netrc file.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableNetrc() : bool;
	
	/**
	 * Excludes the body from the output. Request method is then set to HEAD. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableNoBody() : bool;
	
	/**
	 * Does not excludes the body from the output. Request method is then set
	 * to GET.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableNoBody() : bool;
	
	/**
	 * Disables the progress meter for cURL transfers. Note: PHP automatically
	 * sets this option to TRUE, this should only be changed for debugging
	 * purposes. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableNoProgress() : bool;
	
	/**
	 * Enables progress methers for cURL transfers.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableNoProgress() : bool;
	
	/**
	 * Ignores any cURL function that causes a signal to be sent to the PHP
	 * process. This is turned on by default in multi-threaded SAPIs so timeout
	 * options can still be used. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableNoSignal() : bool;
	
	/**
	 * Does not ignore cURL function that causes a signal to be sent to the PHP
	 * process.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableNoSignal() : bool;
	
	/**
	 * Enables to not handle dot dot sequences. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enablePathAsIs() : bool;
	
	/**
	 * Disables to handle dot dot sequences.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disablePathAsIs() : bool;
	
	/**
	 * Enables to wait for pipelining/multiplexing. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enablePipeWait() : bool;
	
	/**
	 * Disables to wait for pipelining/multiplexing. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disablePipeWait() : bool;
	
	/**
	 * Sets the HTTP request method to GET. Since GET is the default, this is
	 * only necessary if the request method has been changed. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableGet() : bool;
	
	/**
	 * Sets the HTTP request method to POST. This POST is the normal
	 * application/x-www-form-urlencoded kind, most commonly used by HTML forms. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enablePost() : bool;
	
	/**
	 * Sets the HTTP request method to PUT. The file to PUT must be set with
	 * CURLOPT_INFILE and CURLOPT_INFILESIZE. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enablePut() : bool;
	
	/**
	 * Enables sending the initial response in the first packet.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableSaslIr() : bool;
	
	/**
	 * Disables sending the initial response in the first packet.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableSaslIr() : bool;
	
	/**
	 * Enables ALPN in the SSL handshake (if the SSL backend libcurl is built
	 * to use supports it), which can be used to negotiate http2. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableSslAlpn() : bool;
	
	/**
	 * Disables ALPN in the SSL handshake (if the SSL backend libcurl is built
	 * to use supports it), which can be used to negotiate http2. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableSslAlpn() : bool;
	
	/**
	 * Enables NPN in the SSL handshake (if the SSL backend libcurl is built
	 * to use supports it), which can be used to negotiate http2. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableSslNpn() : bool;
	
	/**
	 * Disables NPN in the SSL handshake (if the SSL backend libcurl is built
	 * to use supports it), which can be used to negotiate http2. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableSslNpn() : bool;
	
	/**
	 * Enables to verify the peer's certificate. Alternate certificates to verify
	 * against can be specified with the CURLOPT_CAINFO option or a certificate
	 * directory can be specified with the CURLOPT_CAPATH option. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableSslVerifyPeer() : bool;
	
	/**
	 * Disables to verify the peer's certificate.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableSslVerifyPeer() : bool;
	
	/**
	 * Enables to verify the certificate's status. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableSslVerifyStatus() : bool;
	
	/**
	 * Disables to verify the certificate's status. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableSslVerifyStatus() : bool;
	
	/**
	 * Enables TCP Fast Open. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableTcpFastOpen() : bool;
	
	/**
	 * Disables TCP Fast Open.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableTcpFastOpen() : bool;
	
	/**
	 * Disables sending of TFTP options requests.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableTftpNoOptions() : bool;
	
	/**
	 * Enables sending of TFTP options requests.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableTftpNoOptions() : bool;
	
	/**
	 * Enables to use ASCII mode for FTP transfers. For LDAP, it retrieves data
	 * in plain text instead of HTML. On Windows systems, it will not set STDOUT
	 * to binary mode. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableTransferText() : bool;
	
	/**
	 * Disables to use ASCII mode.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableTransferText() : bool;
	
	/**
	 * Enables to keep sending the username and password when following locations
	 * (using CURLOPT_FOLLOWLOCATION), even when the hostname has changed. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableUnrestrictedAuth() : bool;
	
	/**
	 * Disables to keep sending username and password when following locations
	 * changed.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableUnrestrictedAuth() : bool;
	
	/**
	 * Enables to output verbose information. Writes output to STDERR, or the
	 * file specified using CURLOPT_STDERR. 
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function enableVerbose() : bool;
	
	/**
	 * Disables to output verbose information to STDERR channel.
	 * 
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function disableVerbose() : bool;
	
	/**
	 * The size of the buffer to use for each read. There is no guarantee this
	 * request will be fulfilled, however. Defaults is 8M.
	 * 
	 * @param integer $bufferSizeBytes
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setBufferSize(int $bufferSizeBytes) : bool;
	
	/**
	 * The number of seconds to wait while trying to connect. Use 0 to wait
	 * indefinitely. Defaults is 0.
	 * 
	 * @param integer $timeoutSeconds
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setConnectionTimeout(int $timeoutSeconds) : bool;
	
	/**
	 * The number of milliseconds to wait while trying to connect. Use 0 to
	 * wait indefinitely. If libcurl is built to use the standard system name
	 * resolver, that portion of the connect will still use full-second
	 * resolution for timeouts with a minimum timeout allowed of one second.
	 * Defaults is 0.
	 * 
	 * @param integer $timeoutMilliSeconds
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setConnectionTimeoutMs(int $timeoutMilliSeconds) : bool;
	
	/**
	 * The number of seconds to keep DNS entries in memory. This option is set
	 * to 120 (2 minutes) by default. 
	 * 
	 * @param integer $timeoutSeconds
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setDnsCacheTimeout(int $timeoutSeconds) : bool;
	
	/**
	 * The timeout for Expect: 100-continue responses in milliseconds. Defaults
	 * to 1000 milliseconds. 
	 * 
	 * @param integer $timeoutMilliSeconds
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setExpect100TimeoutMs(int $timeoutMilliSeconds) : bool;
	
	/**
	 * The FTP authentication method (when is activated): CURLFTPAUTH_SSL
	 * (try SSL first), CURLFTPAUTH_TLS (try TLS first), or CURLFTPAUTH_DEFAULT
	 * (let cURL decide).
	 * 
	 * @param CurlFtpSslAuthMethodInterface $ftpSslAuthMethod
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setFtpSslAuth(CurlFtpSslAuthMethodInterface $ftpSslAuthMethod) : bool;
	
	/**
	 * How to deal with headers. One of the following constants: 
	 * CURLHEADER_UNIFIED: the headers specified in CURLOPT_HTTPHEADER will be 
	 * 		used in requests both to servers and proxies. With this option
	 * 		enabled, CURLOPT_PROXYHEADER will not have any effect.
	 * CURLHEADER_SEPARATE: makes CURLOPT_HTTPHEADER headers only get sent to a
	 * 		server and not to a proxy. Proxy headers must be set with
	 * 		CURLOPT_PROXYHEADER to get used. Note that if a non-CONNECT
	 * 		request is sent to a proxy, libcurl will send both server headers
	 * 		and proxy headers. When doing CONNECT, libcurl will send
	 * 		CURLOPT_PROXYHEADER headers only to the proxy and then
	 * 		CURLOPT_HTTPHEADER headers only to the server.
	 * Defaults to CURLHEADER_SEPARATE as of cURL 7.42.1, and
	 * CURLHEADER_UNIFIED before. 
	 * 
	 * @param CurlHeaderOptionInterface $optionMask
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setHeaderOpt(CurlHeaderOptionInterface $optionMask) : bool;
	
	/**
	 * CURL_HTTP_VERSION_NONE (default, lets CURL decide which version to use),
	 * CURL_HTTP_VERSION_1_0 (forces HTTP/1.0), or CURL_HTTP_VERSION_1_1
	 * (forces HTTP/1.1). 
	 * 
	 * @param CurlHttpVersionInterface $httpVersion
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setHttpVersion(CurlHttpVersionInterface $httpVersion) : bool;
	
	/**
	 * The HTTP authentication method(s) to use. The options are: CURLAUTH_BASIC,
	 * CURLAUTH_DIGEST, CURLAUTH_GSSNEGOTIATE, CURLAUTH_NTLM, CURLAUTH_ANY,
	 * and CURLAUTH_ANYSAFE. The bitwise | (or) operator can be used to combine
	 * more than one method. If this is done, cURL will poll the server to see
	 * what methods it supports and pick the best one.
	 * CURLAUTH_ANY is an alias for CURLAUTH_BASIC | CURLAUTH_DIGEST | 
	 * CURLAUTH_GSSNEGOTIATE | CURLAUTH_NTLM. CURLAUTH_ANYSAFE is an alias for
	 * CURLAUTH_DIGEST | CURLAUTH_GSSNEGOTIATE | CURLAUTH_NTLM. 
	 * 
	 * @param CurlHttpAuthMethodInterface $authMethod
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setHttpAuth(CurlHttpAuthMethodInterface $authMethod) : bool;
	
	/**
	 * The expected size, in bytes, of the file when uploading a file to a 
	 * remote site. Note that using this option will not stop libcurl from
	 * sending more data, as exactly what is sent depends on CURLOPT_READFUNCTION. 
	 * 
	 * @param integer $fileSizeBytes
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setInFileSize(int $fileSizeBytes) : bool;
	
	/**
	 * The transfer speed, in bytes per second, that the transfer should be
	 * below during the count of CURLOPT_LOW_SPEED_TIME seconds before PHP
	 * considers the transfer too slow and aborts. 
	 * 
	 * @param integer $byteSize
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setLowSpeedLimit(int $byteSize) : bool;
	
	/**
	 * The number of seconds the transfer speed should be below 
	 * CURLOPT_LOW_SPEED_LIMIT before PHP considers the transfer too slow and
	 * aborts. 	
	 * 
	 * @param integer $seconds
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setLowSpeedTime(int $seconds) : bool;
	
	/**
	 * The maximum amount of persistent connections that are allowed. When the
	 * limit is reached, CURLOPT_CLOSEPOLICY is used to determine which
	 * connection to close. 
	 * 
	 * @param integer $nbConnects
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setMaxConnects(int $nbConnects) : bool;
	
	/**
	 * The maximum amount of HTTP redirections to follow. Use this option
	 * alongside CURLOPT_FOLLOWLOCATION. 
	 * 
	 * @param integer $nbRedirs
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setMaxRedirs(int $nbRedirs) : bool;
	
	/**
	 * An alternative port number to connect to. 
	 * 
	 * @param integer $port
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setPort(int $port) : bool;
	
	/**
	 * A bitmask of 1 (301 Moved Permanently), 2 (302 Found) and 4 (303 See
	 * Other) if the HTTP POST method should be maintained when 
	 * CURLOPT_FOLLOWLOCATION is set and a specific type of redirect occurs. 
	 * 
	 * @param CurlPostRedirectionInterface $postRedir
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setPostRedir(CurlPostRedirectionInterface $postRedir) : bool;
	
	/**
	 * Bitmask of CURLPROTO_* values. If used, this bitmask limits what
	 * protocols libcurl may use in the transfer. This allows you to have a
	 * libcurl built to support a wide range of protocols but still limit
	 * specific transfers to only be allowed to use a subset of them. By
	 * default libcurl will accept all protocols it supports. See also
	 * CURLOPT_REDIR_PROTOCOLS.
	 * 
	 * Valid protocol options are: CURLPROTO_HTTP, CURLPROTO_HTTPS, 
	 * CURLPROTO_FTP, CURLPROTO_FTPS, CURLPROTO_SCP, CURLPROTO_SFTP, 
	 * CURLPROTO_TELNET, CURLPROTO_LDAP, CURLPROTO_LDAPS, CURLPROTO_DICT, 
	 * CURLPROTO_FILE, CURLPROTO_TFTP, CURLPROTO_ALL 
	 * 
	 * @param CurlProtocolInterface $protocol
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setProtocols(CurlProtocolInterface $protocol) : bool;
	
	/**
	 * The HTTP authentication method(s) to use for the proxy connection. Use
	 * the same bitmasks as described in CURLOPT_HTTPAUTH. For proxy
	 * authentication, only CURLAUTH_BASIC and CURLAUTH_NTLM are currently
	 * supported. 
	 * 
	 * @param CurlHttpAuthMethodInterface $proxyAuthMethod
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setProxyAuth(CurlHttpAuthMethodInterface $proxyAuthMethod) : bool;
	
	/**
	 * The port number of the proxy to connect to. This port number can also
	 * be set in CURLOPT_PROXY.
	 * 
	 * @param integer $proxyPort
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setProxyPort(int $proxyPort) : bool;
	
	/**
	 * Either CURLPROXY_HTTP (default), CURLPROXY_SOCKS4, CURLPROXY_SOCKS5,
	 * CURLPROXY_SOCKS4A or CURLPROXY_SOCKS5_HOSTNAME. 
	 * 
	 * @param CurlProxyTypeInterface $proxyType
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setProxyType(CurlProxyTypeInterface $proxyType) : bool;
	
	/**
	 * Bitmask of CURLPROTO_* values. If used, this bitmask limits what
	 * protocols libcurl may use in a transfer that it follows to in a
	 * redirect when CURLOPT_FOLLOWLOCATION is enabled. This allows you to
	 * limit specific transfers to only be allowed to use a subset of protocols
	 * in redirections. By default libcurl will allow all protocols except for
	 * FILE and SCP. This is a difference compared to pre-7.19.4 versions which
	 * unconditionally would follow to all protocols supported. See also
	 * CURLOPT_PROTOCOLS for protocol constant values. 
	 * 
	 * @param CurlProtocolInterface $protocol
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setRedirProtocols(CurlProtocolInterface $protocol) : bool;
	
	/**
	 * The offset, in bytes, to resume a transfer from. 
	 * 
	 * @param integer $offsetBytes
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setResumeFrom(int $offsetBytes) : bool;
	
	/**
	 * Set SSL behavior options, which is a bitmask of any of the following
	 * constants: 
	 * CURLSSLOPT_ALLOW_BEAST: do not attempt to use any workarounds for a 
	 * 		security flaw in the SSL3 and TLS1.0 protocols.
	 * CURLSSLOPT_NO_REVOKE: disable certificate revocation checks for those
	 * 		SSL backends where such behavior is present.
	 * 
	 * @param CurlSslOptionInterface $option
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslOptions(CurlSslOptionInterface $option) : bool;
	
	/**
	 * 1 to check the existence of a common name in the SSL peer certificate.
	 * 2 to check the existence of a common name and also verify that it
	 * matches the hostname provided. 0 to not check the names. In production
	 * environments the value of this option should be kept at 2 (default value). 
	 * 
	 * @param CurlSslVerifyHostMethodInterface $verifyMask
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslVerifyHost(CurlSslVerifyHostMethodInterface $verifyMask) : bool;
	
	/**
	 * One of CURL_SSLVERSION_DEFAULT (0), CURL_SSLVERSION_TLSv1 (1),
	 * CURL_SSLVERSION_SSLv2 (2), CURL_SSLVERSION_SSLv3 (3),
	 * CURL_SSLVERSION_TLSv1_0 (4), CURL_SSLVERSION_TLSv1_1 (5) or 
	 * CURL_SSLVERSION_TLSv1_2 (6). Note: Your best bet is to not set this and
	 * let it use the default. Setting it to 2 or 3 is very dangerous given the
	 * known vulnerabilities in SSLv2 and SSLv3. 
	 * 
	 * @param CurlSslVersionInterface $sslVersion
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslVersion(CurlSslVersionInterface $sslVersion) : bool;
	
	/**
	 * Set the numerical stream weight (a number between 1 and 256). 
	 * 
	 * @param integer $weight
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setStreamWeight(int $weight) : bool;
	
	/**
	 * How CURLOPT_TIMEVALUE is treated. Use CURL_TIMECOND_IFMODSINCE to return
	 * the page only if it has been modified since the time specified in
	 * CURLOPT_TIMEVALUE. If it hasn't been modified, a "304 Not Modified"
	 * header will be returned assuming CURLOPT_HEADER is TRUE. Use 
	 * CURL_TIMECOND_IFUNMODSINCE for the reverse effect. 
	 * CURL_TIMECOND_IFMODSINCE is the default. 
	 * 
	 * @param CurlTimeConditionInterface $timeCondition
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setTimeCondition(CurlTimeConditionInterface $timeCondition) : bool;
	
	/**
	 * The maximum number of seconds to allow cURL functions to execute. 
	 * 
	 * @param integer $timeoutSeconds
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setTimeout(int $timeoutSeconds) : bool;
	
	/**
	 * The maximum number of milliseconds to allow cURL functions to execute.
	 * If libcurl is built to use the standard system name resolver, that
	 * portion of the connect will still use full-second resolution for
	 * timeouts with a minimum timeout allowed of one second. 
	 * 
	 * @param integer $timeoutMilliSeconds
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setTimeoutMs(int $timeoutMilliSeconds) : bool;
	
	/**
	 * The time in seconds since January 1st, 1970. The time will be used by
	 * CURLOPT_TIMECONDITION. By default, CURL_TIMECOND_IFMODSINCE is used. 
	 * 
	 * @param DateTimeInterface $timestamp
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setTimeValue(DateTimeInterface $timestamp) : bool;
	
	/**
	 * If a download exceeds this speed (counted in bytes per second) on
	 * cumulative average during the transfer, the transfer will pause to keep
	 * the average rate less than or equal to the parameter value. Defaults to
	 * unlimited speed. 
	 * 
	 * @param integer $maxSpeedBytesPerSec
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setMaxRecvSpeedLarge(int $maxSpeedBytesPerSec) : bool;
	
	/**
	 * If an upload exceeds this speed (counted in bytes per second) on
	 * cumulative average during the transfer, the transfer will pause to keep
	 * the average rate less than or equal to the parameter value. Defaults to
	 * unlimited speed. 
	 * 
	 * @param integer $maxSpeedBytesPerSec
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setMaxSendSpeedLarge(int $maxSpeedBytesPerSec) : bool;
	
	/**
	 * A bitmask consisting of one or more of CURLSSH_AUTH_PUBLICKEY,
	 * CURLSSH_AUTH_PASSWORD, CURLSSH_AUTH_HOST, CURLSSH_AUTH_KEYBOARD. Set to
	 * CURLSSH_AUTH_ANY to let libcurl pick one. 
	 * 
	 * @param CurlSslAuthTypeInterface $authType
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSshAuthTypes(CurlSslAuthTypeInterface $authType) : bool;
	
	/**
	 * Allows an application to select what kind of IP addresses to use when
	 * resolving host names. This is only interesting when using host names
	 * that resolve addresses using more than one version of IP, possible
	 * values are CURL_IPRESOLVE_WHATEVER, CURL_IPRESOLVE_V4, CURL_IPRESOLVE_V6,
	 * by default CURL_IPRESOLVE_WHATEVER. 
	 * 
	 * @param CurlIpResolveMethodInterface $ipResolvMask
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setIpResolve(CurlIpResolveMethodInterface $ipResolvMask) : bool;
	
	/**
	 * Tell curl which method to use to reach a file on a FTP(S) server. 
	 * Possible values are CURLFTPMETHOD_MULTICWD, CURLFTPMETHOD_NOCWD and
	 * CURLFTPMETHOD_SINGLECWD. 
	 * 
	 * @param CurlFtpFileMethodInterface $fileMethod
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setFtpFileMethod(CurlFtpFileMethodInterface $fileMethod) : bool;
	
	/**
	 * The name of a file holding one or more certificates to verify the peer
	 * with. This only makes sense when used in combination with 
	 * CURLOPT_SSL_VERIFYPEER. 
	 * 
	 * @param string $caFilePath
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setCaInfo(string $caFilePath) : bool;
	
	/**
	 * A directory that holds multiple CA certificates. Use this option
	 * alongside CURLOPT_SSL_VERIFYPEER. 
	 * 
	 * @param string $caDirPath
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setCaPath(string $caDirPath) : bool;
	
	/**
	 * The contents of the "Cookie: " header to be used in the HTTP request.
	 * Note that multiple cookies are separated with a semicolon followed by a
	 * space (e.g., "fruit=apple; colour=red").
	 * 
	 * @param string $cookieString
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setCookie(string $cookieString) : bool;
	
	/**
	 * The name of the file containing the cookie data. The cookie file can be
	 * in Netscape format, or just plain HTTP-style headers dumped into a file.
	 * If the name is an empty string, no cookies are loaded, but cookie
	 * handling is still enabled. 
	 * 
	 * @param string $cookieFilePath
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setCookieFile(string $cookieFilePath) : bool;
	
	/**
	 * The name of a file to save all internal cookies to when the handle is
	 * closed, e.g. after a call to curl_close. 
	 * 
	 * @param string $cookieJarFilePath
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setCookieJarFile(string $cookieJarFilePath) : bool;
	
	/**
	 * A custom request method to use instead of "GET" or "HEAD" when doing a
	 * HTTP request. This is useful for doing "DELETE" or other, more obscure
	 * HTTP requests. Valid values are things like "GET", "POST", "CONNECT"
	 * and so on; i.e. Do not enter a whole HTTP request line here. For
	 * instance, entering "GET /index.html HTTP/1.0\r\n\r\n" would be incorrect.
	 * 
	 * Note: Don't do this without making sure the server supports the custom
	 * request method first. 
	 * 
	 * @param string $request
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setCustomRequest(string $request) : bool;
	
	/**
	 * The default protocol to use if the URL is missing a scheme name.
	 * 
	 * @param string $defaultProtocol
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setDefaultProtocol(string $defaultProtocol) : bool;
	
	/**
	 * Set the name of the network interface that the DNS resolver should bind
	 * to. This must be an interface name (not an address). 
	 * 
	 * @param string $dnsInterface
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setDnsInterface(string $dnsInterface) : bool;
	
	/**
	 * Set the local IPv4 address that the resolver should bind to. The argument
	 * should contain a single numerical IPv4 address as a string. 
	 * 
	 * @param Ipv4AddressInterface $ipv4
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setDnsLocalIp4(Ipv4AddressInterface $ipv4) : bool;
	
	/**
	 * Set the local IPv6 address that the resolver should bind to. The argument
	 * should contain a single numerical IPv6 address as a string. 
	 * 
	 * @param Ipv6AddressInterface $ipv6
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setDnsLocalIp6(Ipv6AddressInterface $ipv6) : bool;
	
	/**
	 * Like CURLOPT_RANDOM_FILE, except a filename to an Entropy Gathering
	 * Daemon socket. 
	 * 
	 * @param string $socketName
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setEgdSocket(string $socketName) : bool;
	
	/**
	 * The contents of the "Accept-Encoding: " header. This enables decoding
	 * of the response. Supported encodings are "identity", "deflate", and
	 * "gzip". If an empty string, "", is set, a header containing all
	 * supported encoding types is sent. 
	 * 
	 * @param string $encodings
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setEncoding(string $encodings) : bool;
	
	/**
	 * The value which will be used to get the IP address to use for the FTP
	 * "PORT" instruction. The "PORT" instruction tells the remote server to
	 * connect to our specified IP address. The string may be a plain IP
	 * address, a hostname, a network interface name (under Unix), or just a
	 * plain '-' to use the systems default IP address. 
	 * 
	 * @param string $port
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setFtpPort(string $port) : bool;
	
	/**
	 * The name of the outgoing network interface to use. This can be an
	 * interface name, an IP address or a host name. 
	 * 
	 * @param string $interface
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setInterface(string $interface) : bool;
	
	/**
	 * The password required to use the CURLOPT_SSLKEY or 
	 * CURLOPT_SSH_PRIVATE_KEYFILE private key. 
	 * 
	 * @param string $password
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setKeyPassword(string $password) : bool;
	
	/**
	 * The KRB4 (Kerberos 4) security level. Any of the following values (in
	 * order from least to most powerful) are valid: "clear", "safe",
	 * "confidential", "private".. If the string does not match one of these,
	 * "private" is used. Setting this option to NULL will disable KRB4
	 * security. Currently KRB4 security only works with FTP transactions. 
	 * 
	 * @param string $kerberosLevel
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setKrb4Level(string $kerberosLevel) : bool;
	
	/**
	 * Can be used to set protocol specific login options, such as the
	 * preferred authentication mechanism via "AUTH=NTLM" or "AUTH=*", and
	 * should be used in conjunction with the CURLOPT_USERNAME option. 
	 * 
	 * @param string $loginOptions
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setLoginOptions(string $loginOptions) : bool;
	
	/**
	 * Set the pinned public key. The string can be the file name of your
	 * pinned public key. The file format expected is "PEM" or "DER". The
	 * string can also be any number of base64 encoded sha256 hashes preceded
	 * by "sha256//" and separated by ";". 
	 * 
	 * @param string $pinnedPublicKey
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setPinnedPublicKey(string $pinnedPublicKey) : bool;
	
	/**
	 * The full data to post in a HTTP "POST" operation. To post a file,
	 * prepend a filename with @ and use the full path. The filetype can be
	 * explicitly specified by following the filename with the type in the
	 * format ';type=mimetype'. This parameter can either be passed as a
	 * urlencoded string like 'para1=val1&para2=val2&...' or as an array with
	 * the field name as key and field data as value. If value is an array,
	 * the Content-Type header will be set to multipart/form-data. As of PHP
	 * 5.2.0, value must be an array if files are passed to this option with
	 * the @ prefix. As of PHP 5.5.0, the @ prefix is deprecated and files can
	 * be sent using CURLFile. The @ prefix can be disabled for safe passing
	 * of values beginning with @ by setting the CURLOPT_SAFE_UPLOAD option to
	 * TRUE. 
	 * 
	 * @param string $postFields
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setPostFields(string $postFields) : bool;
	
	/**
	 * Any data that should be associated with this cURL handle. This data can
	 * subsequently be retrieved with the CURLINFO_PRIVATE option of
	 * curl_getinfo(). cURL does nothing with this data. When using a cURL
	 * multi handle, this private data is typically a unique key to identify a
	 * standard cURL handle. 
	 * 
	 * @param string $privateData
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setPrivate(string $privateData) : bool;
	
	/**
	 * The HTTP proxy to tunnel requests through. 
	 * 
	 * @param string $proxyUrl
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setProxy(string $proxyUrl) : bool;
	
	/**
	 * The proxy authentication service name. 
	 * 
	 * @param string $proxyServiceName
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setProxyServiceName(string $proxyServiceName) : bool;
	
	/**
	 * A username and password formatted as "[username]:[password]" to use for
	 * the connection to the proxy. 
	 * 
	 * @param string $proxyUserPassword
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setProxyUserPassword(string $proxyUserPassword) : bool;
	
	/**
	 * A filename to be used to seed the random number generator for SSL. 
	 * 
	 * @param string $filePath
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setRandomFile(string $filePath) : bool;
	
	/**
	 * Range(s) of data to retrieve in the format "X-Y" where X or Y are
	 * optional. HTTP transfers also support several intervals, separated with
	 * commas in the format "X-Y,N-M". 
	 * 
	 * @param string $rangeIntervalList
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setRange(string $rangeIntervalList) : bool;
	
	/**
	 * The contents of the "Referer: " header to be used in a HTTP request. 
	 * 
	 * @param string $referer
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setReferer(string $referer) : bool;
	
	/**
	 * The authentication service name.
	 * 
	 * @param string $authServiceName
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setServiceName(string $authServiceName) : bool;
	
	/**
	 * A string containing 32 hexadecimal digits. The string should be the MD5
	 * checksum of the remote host's public key, and libcurl will reject the
	 * connection to the host unless the md5sums match. This option is only
	 * for SCP and SFTP transfers. 
	 * 
	 * @param string $md5Hash
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSshHostPublicKeyMd5(string $md5Hash) : bool;
	
	/**
	 * The file name for your public key. If not used, libcurl defaults to
	 * $HOME/.ssh/id_dsa.pub if the HOME environment variable is set, and just
	 * "id_dsa.pub" in the current directory if HOME is not set. 
	 * 
	 * @param string $publicKeyFilePath
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSshPublicKeyfile(string $publicKeyFilePath) : bool;
	
	/**
	 * The file name for your private key. If not used, libcurl defaults to
	 * $HOME/.ssh/id_dsa if the HOME environment variable is set, and just
	 * "id_dsa" in the current directory if HOME is not set. If the file is
	 * password-protected, set the password with CURLOPT_KEYPASSWD. 
	 * 
	 * @param string $privateKeyFilePath
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSshPrivateKeyfile(string $privateKeyFilePath) : bool;
	
	/**
	 * A list of ciphers to use for SSL. For example, RC4-SHA and TLSv1 are
	 * valid cipher lists. 
	 * 
	 * @param string $cipherList
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslCipherList(string $cipherList) : bool;
	
	/**
	 * The name of a file containing a PEM formatted certificate. 
	 * 
	 * @param string $pemFileName
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslCert(string $pemFileName) : bool;
	
	/**
	 * The password required to use the CURLOPT_SSLCERT certificate. 
	 * 
	 * @param string $pemFilePasswd
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslCertPasswd(string $pemFilePasswd) : bool;
	
	/**
	 * The format of the certificate. Supported formats are "PEM" (default),
	 * "DER", and "ENG". 
	 * 
	 * @param CurlSslKeyTypeInterface $certType
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslCertType(CurlSslKeyTypeInterface $certType) : bool;
	
	/**
	 * The identifier for the crypto engine of the private SSL key specified in CURLOPT_SSLKEY. 
	 * 
	 * @param string $engine
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslEngine(string $engine) : bool;
	
	/**
	 * The identifier for the crypto engine used for asymmetric crypto operations. 
	 * 
	 * @param string $engine
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslEngineDefault(string $engine) : bool;
	
	/**
	 * The name of a file containing a private SSL key. 
	 * 
	 * @param string $fileName
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslKey(string $fileName) : bool;
	
	/**
	 * The secret password needed to use the private SSL key specified in
	 * CURLOPT_SSLKEY. Note: Since this option contains a sensitive password,
	 * remember to keep the PHP script it is contained within safe. 
	 * 
	 * @param string $passwd
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslKeyPassword(string $passwd) : bool;
	
	/**
	 * The key type of the private SSL key specified in CURLOPT_SSLKEY.
	 * Supported key types are "PEM" (default), "DER", and "ENG". 
	 * 
	 * @param CurlSslKeyTypeInterface $type
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSslKeyType(CurlSslKeyTypeInterface $type) : bool;
	
	/**
	 * Enables the use of Unix domain sockets as connection endpoint and sets
	 * the path to the given string. 
	 * 
	 * @param string $fullPath
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setUnixSocketPath(string $fullPath) : bool;
	
	/**
	 * The URL to fetch.
	 * 
	 * @param UriInterface $uri
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setUrl(UriInterface $uri) : bool;
	
	/**
	 * The contents of the "User-Agent: " header to be used in a HTTP request.
	 * 
	 * @param UserAgentInterface $userAgent
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setUserAgent(UserAgentInterface $userAgent) : bool;
	
	/**
	 * The user name to use in authentication. 
	 * 
	 * @param string $username
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setUsername(string $username) : bool;
	
	/**
	 * A username and password formatted as "[username]:[password]" to use for
	 * the connection. 
	 * 
	 * @param string $userPassword
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setUserPassword(string $userPassword) : bool;
	
	/**
	 * Specifies the OAuth 2.0 access token.
	 * 
	 * @param string $token
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setXOAuthH2Bearer(string $token) : bool;
	
	/**
	 * Connect to a specific host and port instead of the URL's host and port.
	 * Accepts an array of strings with the format : 
	 * HOST:PORT:CONNECT-TO-HOST:CONNECT-TO-PORT. 
	 * 
	 * @param array<integer, string> $bridges
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setConnectTo(array $bridges) : bool;
	
	/**
	 * An array of HTTP 200 responses that will be treated as valid responses
	 * and not as errors. 
	 * 
	 * @param array<integer, integer> $aliases
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setHttp200Aliases(array $aliases) : bool;
	
	/**
	 * An array of HTTP header fields to set, in the format 
	 * ['Content-type: text/plain', 'Content-length: 100'].
	 * 
	 * @param array<integer, string> $headers
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setHttpHeader(array $headers) : bool;
	
	/**
	 * An array of custom HTTP headers to pass to proxies.
	 * 
	 * @param array<integer, string> $headers
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setProxyHeaders(array $headers) : bool;
	
	/**
	 * An array of FTP commands to execute on the server after the FTP request
	 * has been performed. 
	 * 
	 * @param array<integer, string> $commands
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setPostQuote(array $commands) : bool;
	
	/**
	 * An array of FTP commands to execute on the server prior to the FTP request.
	 * 
	 * @param array<integer, string> $commands
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setQuote(array $commands) : bool;
	
	/**
	 * Provide a custom address for a specific host and port pair. An array of
	 * hostname, port, and IP address strings, each element separated by a
	 * colon. In the format: ["example.com:80:127.0.0.1"].
	 * 
	 * @param array<integer, string> $addresses
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setResolve(array $addresses) : bool;
	
	/**
	 * The file that the transfer should be written to. Defaults is STDOUT.
	 * 
	 * @param resource $resource
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setFile($resource) : bool;
	
	/**
	 * The file that the transfer should be read from whe uploading.
	 * 
	 * @param resource $resource
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setInFile($resource) : bool;
	
	/**
	 * Sets the stream where to write errors to. Defaults to STDERR.
	 * 
	 * @param resource $resource
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setStdErr($resource) : bool;
	
	/**
	 * Sets the stream where to write headers. This method accepts a stream
	 * resource structure, or an object that implements.
	 * 
	 * @param resource $resource
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setWriteHeader($resource) : bool;
	
	/**
	 * Sets a header handler for the headers to write for the response.
	 * 
	 * @param CurlHeaderFunctionInterface $callback
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setHeaderFunction(CurlHeaderFunctionInterface $callback) : bool;
	
	/**
	 * Sets a password resolver handler for the calling of password prompts.
	 * 
	 * @param CurlPasswordFunctionInterface $callback
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setPasswordFunction(CurlPasswordFunctionInterface $callback) : bool;
	
	/**
	 * Sets a progress function handler for the download and upload transfers.
	 * 
	 * @param CurlProgressFunctionInterface $callback
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setProgressFunction(CurlProgressFunctionInterface $callback) : bool;
	
	/**
	 * Sets a read function handler for the input files.
	 * 
	 * @param CurlReadFunctionInterface $callback
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setReadFunction(CurlReadFunctionInterface $callback) : bool;
	
	/**
	 * Sets a write function handler for the response.
	 * 
	 * @param CurlWriteFunctionInterface $callback
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setWriteFunction(CurlWriteFunctionInterface $callback) : bool;
	
	/**
	 * Specifies this handle to be shared with the given handle.
	 *
	 * @param CurlSharedInterface $handle
	 * @return boolean true if the option is successfully set, false otherwise
	 */
	public function setSharedHandle(CurlSharedInterface $handle) : bool;
	
}
