<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

use Throwable;

/**
 * CurlThrowable class file.
 * 
 * This Throwable is to differentiate against other Throwables.
 * 
 * @author Anastaszor
 */
interface CurlThrowable extends Throwable
{
	// nothing to add
}
