<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlMultiInterface interface file.
 * 
 * This interface represents the a curl object that is able to fetch multiple
 * web pages asynchronously.
 * 
 * @author Anastaszor
 */
interface CurlMultiInterface
{
	
	/**
	 * Adds a specific handle to this multiple handle.
	 * 
	 * @param CurlInterface $handle
	 * @return boolean
	 * @throws CurlThrowable if the handle is not compatible
	 */
	public function addHandle(CurlInterface $handle) : bool;
	
	/**
	 * Removes a specific handle to this multiple handle.
	 * 
	 * @param CurlInterface $handle
	 * @return boolean
	 * @throws CurlThrowable if the handle is not compatible
	 */
	public function removeHandle(CurlInterface $handle) : bool;
	
	/**
	 * Executes the current curl request.
	 * 
	 * @return boolean
	 * @throws CurlThrowable if the execution failed
	 */
	public function execute() : bool;
	
	/**
	 * Gets the latest error code.
	 * 
	 * @return integer
	 */
	public function getErrorCode() : int;
	
	/**
	 * Gets the latest error message.
	 * 
	 * @return string
	 */
	public function getErrorMessage() : string;
	
	/**
	 * Gets whether curl requests are been made or received.
	 * 
	 * @return boolean
	 */
	public function isStillRunning() : bool;
	
	/**
	 * Gets the current information about the stack of this multiple handle.
	 * 
	 * @return CurlMultiInfoInterface
	 * @throws CurlThrowable if the getting of information failed
	 */
	public function getInfo() : CurlMultiInfoInterface;
	
	/**
	 * Disables the HTTP/1 pipeling and the HTTP/2 multiplexing.
	 * 
	 * @return boolean
	 */
	public function disablePipelining() : bool;
	
	/**
	 * Enables the HTTP/1 pipelining. Enabling pipelining on a multi handle
	 * will make it attempt to perform HTTP Pipelining as far as possible for
	 * transfers using this handle. This means that if you add a second request
	 * that can use an already existing connection, the second request will be
	 * "piped" on the same connection.
	 * 
	 * @return boolean
	 */
	public function enablePipelining() : bool;
	
	/**
	 * As of cURL 7.43.0 this parameter can be used to multiplex the new
	 * transfer over an existing HTTP/2 connection if possible.
	 * 
	 * @return boolean
	 */
	public function enableMultiplexing() : bool;
	
	/**
	 * Pass a number that will be used as the maximum amount of simultaneously
	 * open connections that libcurl may cache. By default the size will be
	 * enlarged to fit four times the number of handles added via
	 * curl_multi_add_handle(). When the cache is full, curl closes the oldest
	 * one in the cache to prevent the number of open connections from
	 * increasing. 
	 * 
	 * @param integer $maxConnections
	 * @return boolean
	 */
	public function setMaxSimultaneousConnections(int $maxConnections) : bool;
	
	/**
	 * Pass a number that specifies the chunk length threshold for pipelining
	 * in bytes.
	 * 
	 * @param integer $sizeInBytes
	 * @return boolean
	 */
	public function setChunkLengthPenaltySize(int $sizeInBytes) : bool;
	
	/**
	 * Pass a number that specifies the size threshold for pipelining penalty
	 * in bytes.
	 * 
	 * @param integer $sizeInBytes
	 * @return boolean
	 */
	public function setContentLengthPenaltySize(int $sizeInBytes) : bool;
	
	/**
	 * Pass a number that specifies the maximum number of connections to a
	 * single host. 
	 * 
	 * @param integer $maxConnections
	 * @return boolean
	 */
	public function setMaxHostConnections(int $maxConnections) : bool;
	
	/**
	 * Pass a number that specifies the maximum number of requests in a
	 * pipeline.
	 * 
	 * @param integer $maxNbRequests
	 * @return boolean
	 */
	public function setMaxPipelineLength(int $maxNbRequests) : bool;
	
	/**
	 * Pass a number that specifies the maximum number of simultaneously open
	 * connections. 
	 * 
	 * @param integer $maxConnections
	 * @return boolean
	 */
	public function setMaxTotalConnections(int $maxConnections) : bool;
	
	/**
	 * Passes a callable that will be registered to handle server pushes.
	 * 
	 * @param CurlMultiPushFunctionInterface $callable
	 * @return boolean
	 */
	public function setPushFunction(CurlMultiPushFunctionInterface $callable) : bool;
	
}
