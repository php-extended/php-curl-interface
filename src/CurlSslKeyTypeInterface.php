<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlSslKeyTypeInterface interface file.
 * 
 * This interface represents an enum about the ssl key types.
 * 
 * @author Anastaszor
 */
interface CurlSslKeyTypeInterface
{
	
	/**
	 * Gets the curl constant value.
	 * 
	 * @return string
	 */
	public function getCurlValue() : string;
	
}
