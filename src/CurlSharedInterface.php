<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlSharedInterface interface file.
 * 
 * This interface represents the a curl object that is able to share specific
 * parameters amongs multiple curl handles.
 * 
 * @author Anastaszor
 */
interface CurlSharedInterface
{
	
	/**
	 * Enables the share of cookie data.
	 * 
	 * @return CurlSharedInterface self
	 */
	public function enableSharedCookies() : CurlSharedInterface;
	
	/**
	 * Disables the share of cookie.
	 * 
	 * @return CurlSharedInterface self
	 */
	public function disableSharedCookies() : CurlSharedInterface;
	
	/**
	 * Enables the share of dns cache. Note that when you use cURL multi
	 * handles, all handles added to the same multi handle will share DNS cache
	 * by default. 
	 * 
	 * @return CurlSharedInterface self
	 */
	public function enableSharedDns() : CurlSharedInterface;
	
	/**
	 * Disables the share of dns cache.
	 * 
	 * @return CurlSharedInterface self
	 */
	public function disableSharedDns() : CurlSharedInterface;
	
	/**
	 * Enables the share of ssl session ids, reducing the time spent on the SSL
	 * handshake when reconnecting to the same server. Note that SSL session
	 * IDs are reused within the same handle by default. 
	 * 
	 * @return CurlSharedInterface self
	 */
	public function enableSharedSsl() : CurlSharedInterface;
	
	/**
	 * Disables the share of ssl session ids.
	 * 
	 * @return CurlSharedInterface self
	 */
	public function disableSharedSsl() : CurlSharedInterface;
	
}
