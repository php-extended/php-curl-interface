<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlReadFunctionInterface interface file.
 * 
 * This interface specifies a callback to read data that is to be transferred
 * via a curl instance.
 * 
 * @author Anastaszor
 */
interface CurlReadFunctionInterface
{
	
	/**
	 * This function executes the reading over the given file, up to maxlength
	 * bytes.
	 * 
	 * @param CurlInterface $curl
	 * @param CurlFileInterface $file
	 * @param integer $maxlength
	 * @return string the binary read data, empty string if eof
	 */
	public function read(CurlInterface $curl, CurlFileInterface $file, int $maxlength) : string;
	
}
