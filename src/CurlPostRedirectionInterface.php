<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlPostRedirectionInterface interface file.
 * 
 * This interface represents the curl post redirection methods that are allowed
 * in curl.
 * 
 * @author Anastaszor
 */
interface CurlPostRedirectionInterface
{
	
	/**
	 * Gets the curl constant value.
	 * 
	 * @return integer
	 */
	public function getCurlValue() : int;
	
	/**
	 * Merges with the other post redirection and returns the result of the merge.
	 * 
	 * @param CurlPostRedirectionInterface $other
	 * @return CurlPostRedirectionInterface
	 */
	public function with(CurlPostRedirectionInterface $other) : CurlPostRedirectionInterface;
	
}
