<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlIpResolveMethodInterface interface file.
 * 
 * This class represents the curl ip resolve methods that are allowed in curl.
 * 
 * @author Anastaszor
 */
interface CurlIpResolveMethodInterface
{
	
	/**
	 * Gets the curl constante value.
	 * 
	 * @return integer
	 */
	public function getCurlValue() : int;
	
}
