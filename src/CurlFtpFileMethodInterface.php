<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlFtpFileMethodInterface interface file.
 * 
 * This interface represents the curl ftp file methods that are allowed in curl.
 * 
 * @author Anastaszor
 */
interface CurlFtpFileMethodInterface
{
	
	/**
	 * Gets the curl constant value.
	 * 
	 * @return integer
	 */
	public function getCurlValue() : int;
	
}
