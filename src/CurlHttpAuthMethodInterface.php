<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-curl-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Curl;

/**
 * CurlHttpAuthMethodInterface interface file.
 * 
 * This interface represents the curl http auth types that are allowed in curl.
 * 
 * @author Anastaszor
 */
interface CurlHttpAuthMethodInterface
{
	
	/**
	 * Gets the curl constant value.
	 * 
	 * @return integer
	 */
	public function getCurlValue() : int;
	
	/**
	 * Merges with the other http auth type and returns the result of the merge.
	 * 
	 * @param CurlHttpAuthMethodInterface $other
	 * @return CurlHttpAuthMethodInterface
	 */
	public function with(CurlHttpAuthMethodInterface $other) : CurlHttpAuthMethodInterface;
	
}
