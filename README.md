# php-extended/php-curl-interface
A library to specify how to wrap curl native methods onto objects

![coverage](https://gitlab.com/php-extended/php-curl-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-curl-interface ^8`


## Basic Usage

This library is an interface-only library.

For a concrete implementation, see [`php-extended/php-curl-object`](https://gitlab.com/php-extended/php-curl-object)


## License

MIT (See [license file](LICENSE)).
